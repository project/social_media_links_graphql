<?php

namespace Drupal\social_media_links_gql\Plugin\GraphQL\Types\SocialMediaLink;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * @GraphQLType(
 *   id = "social_media_platform_value",
 *   name = "SocialMediaPlatformValue",
 * )
 */
class SocialMediaPlatformValue extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function applies($object, ResolveContext $context, ResolveInfo $info) {
    return $object['type'] == 'SocialMediaPlatformValue';
  }

}
