<?php

namespace Drupal\social_media_links_gql\Plugin\GraphQL\Fields\SocialMediaLink;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve a link fields route object.
 *
 * @GraphQLField(
 *   id = "social_media_link_channel",
 *   secure = true,
 *   name = "channel",
 *   parents = {"SocialMediaPlatformValue"},
 *   type = "String",
 * )
 */
class SocialMediaLinkChannel extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['channel'];

  }

}
