<?php

namespace Drupal\social_media_links_gql\Plugin\GraphQL\Fields\SocialMediaLink;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\social_media_links\SocialMediaLinksPlatformManager;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve a link fields route object.
 *
 * @GraphQLField(
 *   id = "social_media_link_url",
 *   secure = true,
 *   name = "url",
 *   parents = {"SocialMediaPlatformValue"},
 *   type = "String",
 * )
 */
class SocialMediaLinkUrl extends FieldPluginBase  implements ContainerFactoryPluginInterface {
  protected $socialPluginManager;

  protected $platforms;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.social_media_links.platform')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SocialMediaLinksPlatformManager $social_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->socialPluginManager = $social_plugin_manager;

    $this->platforms = $social_plugin_manager->getPlatforms();
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {

    if (empty($this->platforms[$value['channel']]['urlPrefix'])) {
      return '';
    }

    yield $this->platforms[$value['channel']]['urlPrefix'] . $value['value'];

  }

}
