<?php

namespace Drupal\social_media_links_gql\Plugin\GraphQL\Fields\SocialMediaLink;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve a link fields route object.
 *
 * @GraphQLField(
 *   id = "social_media_link_value",
 *   secure = true,
 *   name = "value",
 *   parents = {"SocialMediaPlatformValue"},
 *   type = "String",
 * )
 */
class SocialMediaLinkValue extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['value'];

  }

}
