<?php

namespace Drupal\social_media_links_gql\Plugin\GraphQL\Fields\SocialMediaLink;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\link\LinkItemInterface;
use Drupal\social_media_links_field\Plugin\Field\FieldType\SocialMediaLinksFieldItem;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve a link fields route object.
 *
 * @GraphQLField(
 *   id = "social_media_link_item_platform_values",
 *   secure = true,
 *   name = "PlatformValues",
 *   type = "[SocialMediaPlatformValue]",
 *   field_types = {"social_media_links_field"},
 *   deriver = "Drupal\graphql_core\Plugin\Deriver\Fields\EntityFieldPropertyDeriver"
 * )
 */
class SocialMediaLinkPlatformValues extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof SocialMediaLinksFieldItem) {

      $value = $value->getValue();
      foreach ($value['platform_values'] as $channel => $value) {
        $a=2;
        yield [
          'channel' => $channel,
          'value' => $value['value'],
          'type' => 'SocialMediaPlatformValue',
        ];
      }

    }
  }

}
